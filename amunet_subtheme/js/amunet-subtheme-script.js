/**
 * @file
 * This file is to add any custom js for the amunet sub-theme.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.amunetSubthemeBehavior = {
      // Perform jQuery as normal in here.
  };

})(jQuery);
