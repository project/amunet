# INTRODUCTION
--------------------------------------------------------------------------------

Amunet is a Sub-Theme for Drupal8 W3CSS Theme.
Amunet Theme is a sub-theme with an invisible vertical menu. Menu button on the
left and search icon on right.


# REQUIREMENTS
--------------------------------------------------------------------------------

This sub-theme require durpal8_w3css_theme to be installed first.


# INSTALLATION
--------------------------------------------------------------------------------

Download the theme to www.your-drupal-site-name/themes/contrib and go to
your-site-domain/admin/appearance and scroll to the bottom until you see Amunet
Theme and click on Install and set as default and click save.


# FEATURES
--------------------------------------------------------------------------------
This sub-theme will inherit all the features from the parent theme.
https://www.drupal.org/project/d8w3css#theme-features

# CONFIGURATION
--------------------------------------------------------------------------------

This theme will use the same configuration from the parent theme.

# HELPFUL LINKS
--------------------------------------------------------------------------------
www.drupal.org/project/d8w3css
www.drupal.org/node/2866181
www.flex6.com/w3css-predefined-color-themes
www.youtube.com/watch?v=WjUOT8ePWQo&list=PLZVSLeSmRrd1eclpwYXHaPgE4cSzwSSGn

www.flex6.com/
www.amun.flashwebcenter.com/
www.anhur.flashwebcenter.com/
www.amunet.flashwebcenter.com/
